<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nyx
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
<meta name="description" content="Discover NYX Cosmetics Australia professional makeup & beauty products today and click to shop all of our new products, best sellers plus much more with Priceline!"/>
<meta name="keywords" content="NYX, NYX Cosmetics, NYX Cosmetics Australia, NYX Cosmetics Professional Makeup, Makeup, Beauty, Professional, Beauty Products, Australia"/>
<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
<link href="<?php bloginfo('template_url'); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php bloginfo('template_url'); ?>/assets/css/style.css" rel="stylesheet">

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-84081521-1', 'auto');
  ga('send', 'pageview');

</script>

<?php wp_head(); ?>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-md-12 text-center logo"> <a href="<?php bloginfo('home'); ?>"><img alt="NYX Cosmetics Logo" src="<?php bloginfo('template_url'); ?>/assets/images/logo.png"></a> </div>
  </div>
