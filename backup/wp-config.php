<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'IndonesiaDB');

/** MySQL database username */
define('DB_USER', 'IDuser');

/** MySQL database password */
define('DB_PASSWORD', '5j3Hg2sh');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ByM:`y-|X:7zN]z!?SZEueD@![8o(2)+wH>9#IXSBn3#/y&#!RX:P(C2?geePtI[');
define('SECURE_AUTH_KEY',  'p1s>yI/[*@4z_YNmvw-(Z<j/6mnic7g^9J{&+Z[B<Mb3B9|+ls9(%$ANY<5TUtax');
define('LOGGED_IN_KEY',    'cWX{V.^JAZ@=`uEuC$[1V>J]Jgtrx8*D2agjwKZkku?K]7M!D;%GE#Vcdo.v:!kr');
define('NONCE_KEY',        ')9jX /$RG,v~ky:sq{:}vO:a/g<cviZ)Ch&[LyRtrm;QXboO7y bzn E1tHeg7Zu');
define('AUTH_SALT',        '}ac+m8HWIO/_{a*`&|3UJ#B^n{f47!fGWUCJpKC8h[X@F~v) #(I@zjYuOT^BTV^');
define('SECURE_AUTH_SALT', '|N-,)B2^AdJE?<}3SQ;2Au+5rg:0Q/{GG/kX~`7e&`a >Bv)Z,J(}Z-AU2C!zM[T');
define('LOGGED_IN_SALT',   ':0OS&|0EMS<x^r#:OAeTY!vgA!2yT!PQDXd!&b=K2:9B^` VF!Jp1H26OA%|um{{');
define('NONCE_SALT',       'R9%W&]&`6<JjJ-s&cS,WRci&6Go2t`gbc;/^%jBt9<8Kp)(iw4A}=L4]^tBUxxW;');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
