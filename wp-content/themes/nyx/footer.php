<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nyx
 */

?>

<div class="container">
	<div class="row">
    <div class="col-md-3">
      <ul class="list-inline">
        <li><a href="https://www.facebook.com/NyxCosmeticsIndonesia/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/assets/images/fb.png" width="55" height="55" ></a></li>
        <li><a href="https://www.instagram.com/nyxcosmetics_indonesia/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/assets/images/inst.png" width="55" height="55" ></a></li>
        <li><a href="https://twitter.com/NyxCosmetics_ID" target="_blank"><img src="<?php bloginfo('template_url'); ?>/assets/images/twitter-1.png" width="55" height="55" ></a></li>
        <li><a href="mailto:consumeradvisory@loreal.com"><img src="<?php bloginfo('template_url'); ?>/assets/images/email.png" width="55" height="55" ></a></li>
      </ul>
    </div>
    <div class="col-md-6 text-center" style="padding-top:20px;">
      <ul class="list-inline">
        <li>© 2016 NYX COSMETICS</li>
        <?php  wp_nav_menu_no_ul(); ?>
        <li>CALL US: 0-800-1-567-325</li>
      </ul>
    </div>
    <div class="col-md-3 "> <img src="<?php bloginfo('template_url'); ?>/assets/images/nyx-goddess.jpg"  > </div>
  </div>
</div>
<script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.min.js"></script> 
<script src="<?php bloginfo('template_url'); ?>/assets/js/bootstrap.min.js"></script> 
<script src="<?php bloginfo('template_url'); ?>/assets/js/scripts.js"></script>

<?php wp_footer(); ?>

</body>
</html>
