<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nyx
 */

get_header(); ?>

	<div class="row">
    <div class="col-md-12 text-center" style="margin-bottom:20px;">
      <h1>NYX WEBSITE COMING SOON <br />
        IN THE MEANTIME...</h1>
    </div>
  </div>
  <!-- BLOCK 1 -->
  <div class="row">
    <div class="col-md-6 padding0 ">
      <div class="col-sm-12">
        <div class="frontpage_square text-center blackbg">
          <div class="signup">
            <h3>STAY UP TO DATE WITH THE HOTTEST NEWS</h3>
            <h1>JOIN THE NYX COMMUNITY</h1>
            <form action="//lazada.us13.list-manage.com/subscribe/post?u=0cb5254b1ef60e6646222a924&amp;id=183502014e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
              <div class="input-group col-xs-10 col-centered" style="margin:20px auto;">
                <input type="email" value="" name="EMAIL" class="email form-control" id="mce-EMAIL" placeholder="email address" required>
                <span class="input-group-btn ">
                <button class="btn btn-default" name="subscribe" type="button">SUBMIT</button>
                </span> </div>
              <div class="checkbox col-xs-10 col-centered" style="color:#FFF; font-size:14px; line-height: 18px;">
                <!-- <label>
                  <input type="checkbox" value="">
                  I have read and agree to the <a href="<?php bloginfo('home'); ?>/privacy" target="_blank" style="color:#FFF; font-size:14px; line-height: 18px; text-decoration:underline;" >Privacy Notice</a> and the <a href="<?php bloginfo('home'); ?>/terms-of-use" target="_blank" style="color:#FFF; font-size:14px; line-height: 18px; text-decoration:underline;" >Terms of Use. </a> </label> -->
                  <label>
                  <input type="checkbox" value="">
                  I have read and agree to the <a href="<?php bloginfo('home'); ?>/?page_id=6" target="_blank" style="color:#FFF; font-size:14px; line-height: 18px; text-decoration:underline;" >Privacy Notice</a> and the <a href="<?php bloginfo('home'); ?>/?page_id=2" target="_blank" style="color:#FFF; font-size:14px; line-height: 18px; text-decoration:underline;" >Terms of Use. </a> </label>
              </div>
            </form>
          </div>
        </div>
        </a> 
	  </div>
    </div>
    <div class="col-md-6 padding0">
      <div class="col-sm-6 col-md-6 col-xs-6" >
        <div class="thumbnail">
          <div class="frontpage_square text-center">
            <h2>BE BOLD</h2>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-6 col-xs-6">
        <div class="thumbnail white-bg">
          <div class="frontpage_square"> <img src="<?php bloginfo('template_url'); ?>/assets/images/img1.jpg" class="img-responsive" > </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-6 col-xs-6">
        <div class="thumbnail white-bg">
          <div class="frontpage_square"> <img src="<?php bloginfo('template_url'); ?>/assets/images/img3.jpg" class="img-responsive" > </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-6 col-xs-6">
        <div class="thumbnail white-bg no-border">
          <div class="frontpage_square"> <img src="<?php bloginfo('template_url'); ?>/assets/images/img4.jpg" class="img-responsive" > </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END BLOCK 1 --> 
  
  <!-- BLOCK 2 -->
  <div class="row">
    <div class="col-md-6 padding0">
      <div class="col-sm-6 col-md-6 col-xs-6" >
        <div class="thumbnail">
          <div class="frontpage_square text-center">
			<a href="https://twitter.com/NyxCosmetics_ID" target="_blank">
            <h2>FOLLOW US</h2>
			</a>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-6 col-xs-6"> <a href="https://www.instagram.com/nyxcosmetics_indonesia/" class="thumbnail white-bg" target="_blank">
        <div class="frontpage_square"> <img src="<?php bloginfo('template_url'); ?>/assets/images/insta.png" class="img-responsive" > </div>
        </a> </div>
      <div class="col-sm-6 col-md-6 col-xs-6"> <a href="https://www.facebook.com/NyxCosmeticsIndonesia/" class="thumbnail white-bg" target="_blank">
        <div class="frontpage_square"> <img src="<?php bloginfo('template_url'); ?>/assets/images/fb.jpg" class="img-responsive" > </div>
        </a> </div>
      <div class="col-sm-6 col-md-6 col-xs-6">
        <div class="thumbnail white-bg no-border">
          <div class="frontpage_square"> <img src="<?php bloginfo('template_url'); ?>/assets/images/img2.jpg" class="img-responsive" > </div>
        </div>
      </div>
    </div>
    <div class="col-md-6 padding0">
      <div class="col-xs-12">
        <div href="#" class="thumbnail no-border" target="_blank">
          <div class="frontpage_square"> <img src="<?php bloginfo('template_url'); ?>/assets/images/img8.jpg" class="img-responsive" style="width:100%;" > </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END BLOCK 2 --> 
  
  <!-- BLOCK 3 -->
  <div class="row">
    <div class="col-md-3 padding0 white-bg">
      <div class="col-sm-6 col-md-12 col-xs-6">
        <div class="thumbnail">
          <div class="frontpage_square"> <img src="<?php bloginfo('template_url'); ?>/assets/images/img7.jpg" class="img-responsive" > </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-12 col-xs-6 white-bg">
        <div class="thumbnail">
          <div class="frontpage_square"> <img src="<?php bloginfo('template_url'); ?>/assets/images/img6.jpg" class="img-responsive" > </div>
        </div>
      </div>
    </div>
    <div class="col-md-6 padding0">
      <div class="col-xs-12"  > <a href="http://www.lazada.co.id/nyx-sis/" class="thumbnail" style="background-color:#ec008c; border:1px solid #ec008c;" target="_blank">
        <div class="frontpage_square" style="background-color:#ec008c;"> <img src="<?php bloginfo('template_url'); ?>/assets/images/lazada.png" class="img-responsive" style="max-width:300px; height:auto; text-align:center; margin:50% auto; top: -50px; left: 0; right: 0;"> </div>
        </a> </div>
    </div>
    <div class="col-md-3 padding0">
      <div class="col-sm-6 col-md-12 col-xs-6"> <a href="http://www.lazada.co.id/nyx-sis/" class="thumbnail" target="_blank">
        <div class="frontpage_square text-center">
          <h2>SHOP NOW</h2>
        </div>
        </a> </div>
      <div class="col-sm-6 col-md-12 col-xs-6">
        <div class="thumbnail  no-border" target="_blank">
          <div class="frontpage_square"> <img src="<?php bloginfo('template_url'); ?>/assets/images/img5.jpg" class="img-responsive" > </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END BLOCK 3 -->

<?php
get_footer();
