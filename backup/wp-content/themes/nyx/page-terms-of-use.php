<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nyx
 */

get_header(); ?>

	<div class="row">
    <div class="col-md-12">
    <!-- <h1><?php //the_title(); ?></h1> -->
    	<?php if ( have_posts() ) : while( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
    	<?php endwhile; endif; ?>  
    </div>
  </div>
</div>
<!-- END BLOCK 3 -->

<?php
get_footer();
